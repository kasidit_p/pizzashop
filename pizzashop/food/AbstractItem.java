package pizzashop.food;

public abstract class AbstractItem implements OrderItem{
	private int size;
	public AbstractItem(int size){
		this.size = size;
	}
	public double getPrice(double [] prices) {
		double price = 0;
		if ( size >= 0 && size < prices.length ) price = prices[size];
		return price;
	}

	public int getSize(){
		return size;
	}
//	public Object clone(){
//		
//	}
}
